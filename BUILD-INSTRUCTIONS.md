### BUILD INSTRUCTIONS

Use your favorite method to acquire the source code for URM.

Assuming the project directory name is urm, follow the commands below:


```
cd urm
mkdir Debug
mkdir Release
```

**To make a Debug Target**

```
cd Debug
cmake -DCMAKE_BUILD_TYPE=Debug ..
make
```
`The target is in build directory`

>If you have trouble compiling switch target to C++20 instead of C++23. You can also use earlier versions of cmake.
<br />`set(CMAKE_CXX_STANDARD 20)` in CMakeLists.txt

**To make a Release Target**
```
cd Debug
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```
`The target is in build directory`

The built targets' directory contents `./urm/<BUILD_TYPE>/build/`:

1. urm
2. urm.lexer
3. urm.parser

Test files are located in `./urm/<BUILD_TYPE>/build/tests/`



**To run ctest**

run the ctest in `./urm/<BUILD_TYPE>/` and not in `./urm/<BUILD_TYPE>/build/`


** Notes: **
1. Please see README.md for more information