cmake_minimum_required (VERSION 3.18.1)

project (URM)

if (CMAKE_BUILD_TYPE STREQUAL "Debug")
    message("debug mode")
else()
    message("release mode")
endif ()



set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/build)
enable_testing()
add_subdirectory(src)
