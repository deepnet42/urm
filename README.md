# Unlimited Register Machine

## Introduction
The Unlimited Register Machine (URM) as implemented per specification from Computability: An introduction to recursive function theory by N.J. Cutland. This is a modification of machine originally based on Shepherdson and Sturgis's model (1963). You can read more about a URM [here](https://www.deepnet42.com/post/2023/02/10/an-unlimited-register-machine/).


### Technical Specification
This implementation tries to be as faithful as possible to the definition in the book. 

The URM has unlimited, hence the name, number of registers. The registers are labeled as *R1,R2,R3,...,Rn*. The registers store natural numbers (**N**) including zero (0). 

A program *P* consists of a list of instructions and *P* is of finite size.

The four (4) main instructions are *Zero*, *Successor*, *Jump*, and *Transfer*. See below for exact description of each instruction.

A program *P* begins execution from instruction line *q* where *q = 1* and proceeds in a sequential manner unless a branching jump is made to a new line *r*. A Program *P* terminates when no more instructions remain to be executed -- that is when execution reaches line *q* such that *q > |P|* (length of *P*).

For convenience, a program *P* can be halted at any point of time by a Jump instruction address zero (0). For example:

```
.
.
.
J(100,100,0) <- this will terminate the program (assuming R100 = R100)
S(2)
T(2,3)
.
.
.
```


## The URM Instruction Set 


| Instruction Name	| Instruction	| Meaning | Pseudocode |
|-----------------------|---------------|---------|------------|
|ZERO                   |Z(n)           |Set register numbered n to the value of ZERO (0) | Rn ← 0 |
|SUCCESSOR		|S(n)           |Increment register numbered n by ONE (1) | Rn ← Rn + 1 |
|JUMP                   |J(m,n,q)       |Jump to instruction numbered q if m==n, otherwise proceed to the next instruction | if (Rm=Rn) ip ← q; otherwise ip ← ip + 1 |
|TRANSFER               | T(m,n)        |Set the value of register numbered n to the value of the register numbered m. | Rn ← Rm |



## Build

See [BUILD-INSTRUCTIONS.md](BUILD-INSTRUCTIONS.md)


## Usage
To run URM programs: 

`usage : ./urm code_file register_file`


Utilities for testing the lexer and parser.
```
urm.lexer source.urm
urm.parser source.urm registers.rf
```

## Sample Output
```
$cat test.urm test.rf
J(1,2,6)
S(2)
S(3)
J(1,2,6)
J(1,1,2)
T(3,1)

9 7 0 0 0 0 0 0

$/urm ./tests/test.urm ./tests/test.rf
### Unlimited Register Machine (URM) ###
© 2022 DeepNet42. All rights reserved.
(config: REGISTER_FILE_SIZE: 256, CODE_LENGTH: 1024)
READY.
 -REGISTER FILE = {9 7 0 0 0 0 0 0}
 -CODE LISTING = {
       J(1,2,6)
       S(2)
       S(3)
       J(1,2,6)
       J(1,1,2)
       T(3,1)
  }
 -LOADED OK.
[ 1] J(1,2,6)                 ip  ← 2 (Execute next instruction)
[ 2] S(2)                 R2 ← R2 + 1 (R2==8)
[ 3] S(3)                 R3 ← R3 + 1 (R3==1)
[ 4] J(1,2,6)                 ip  ← 5 (Execute next instruction)
[ 5] J(1,1,2)                 ip  ← 2 (Jump to line # 2)
[ 2] S(2)                 R2 ← R2 + 1 (R2==9)
[ 3] S(3)                 R3 ← R3 + 1 (R3==2)
[ 4] J(1,2,6)                 ip  ← 6 (Jump to line # 6)
[ 6] T(3,1)                 R3 ← R1 (R1==2)
 -HALT.

R[1] = 2
R[2] = 9
R[3] = 2


stats { ticks:9 e { Z:0 S:4 T:1 J:4} }

done.
```


## Notes

### Language
The URM Virtual Machine is written in C++. No 3rd party libraries are required. CMake is used to build the project. Testing is available via ctest.


### Other Simulators
The following links below are 3rd party online simulators/vms. The vm in this project is mostly compatible with the ones below:

+ http://sites.oxy.edu/rnaimi/home/URMsim.htm
+ https://www.math.ucla.edu/~bonsoon/urm.html
+ https://sean.mcgivern.me.uk/urm-evaluator/



### Referenced Book

*Computability: An Introduction to Recursive Function Theory* by Nigel L. Cutland


## Other



### Supported Features
+ [x] Line numbers
+ [x] Comments

### To be determined
+ [ ] Additional testing files
+ [ ] Better testing support and verification of test results
+ [ ] Better output formatting
+ [ ] Label support

### Non portable features
The features below are **specific** to this URM VM. They may not work with other VMs or simulators. The original machine as described by Nigel L. Cutland in his book, *Computability: An Introduction to Recursive Function Theory*, does not support any of the features below.


#### Numbered Lines

Numbered lines are a purely **cosmetic** option. The parser **does not** verify the correctness and numbering order. It is strictly a convenience feature. These are **not** labels. Do not use them as labels. Support for labels may come in the future. 

e.g.
```
1:J(1,2,6)
2:S(2)
3:S(3)
4:J(1,2,6)
5:J(1,1,2)
6:T(3,1)
```


#### Comments
Comments begin with a semicolon symbol `;` and terminated by end of line `\n`. Empty comments are supported. Multi line comments are not supported. 

e.g.
```
; A simple example of a urm program
; \file test.urm
; \version 0
; \date 2022
;
1:J(1,2,6)        ; Jump to line 6 if Register 1 == Register 2
2:S(2)
3:S(3)
4:J(1,2,6)
5:J(1,1,2)
6:T(3,1)
```


---

(c) 2022 DeepNet42. All Rights Reserved.

Updated Version.


https://www.deepnet42.com
https://www.deepnet42.com/projects/#unlimited-register-machine

```
______  _______ _______  _____  __   _ _______ _______     
|     \ |______ |______ |_____] | \  | |______    |        
|_____/ |______ |______ |       |  \_| |______    |        
                                                           
Deepnet42
```
