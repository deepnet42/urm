/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file URM.cpp
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */


# include "vm.h"
# include "isa.h"
# include "isa.config.h"
# include "lexer.h"
# include "parser.h"

# include <iostream>
# include <vector>
# include <string>
# include <fstream> 
# include <iomanip> 



using namespace winkieface;

void BANNER();

int main(int argc, char* argv[]) {


	

	if(argc < 3) {
		std::cout << "usage : " << argv[0] << " code_file register_file\n";
		return EXIT_FAILURE;
	}

	BANNER();
	


	std::ifstream code_file(argv[1]);
	std::ifstream register_file(argv[2]);

	if(!code_file and !code_file.is_open()) {
		std::cerr << "error: cannot open file named " << argv[1] << "\n";
		return EXIT_FAILURE;
	}

	if(!register_file and !register_file.is_open()) {
		code_file.close();
		std::cerr << "error: cannot open file named " << argv[1] << "\n";
		return EXIT_FAILURE;
	}


	std::vector<int> registers;
	int value;
	try {
		while(register_file >> value) {
			registers.push_back(value);
		}
	} catch (...) {
		std::cerr << "exception: bad register_file!\n";
		code_file.close();
		register_file.close();
		return EXIT_FAILURE;
	}

	
	VMEngine e;
	lexer lexer(&code_file);
	parser p(&lexer);
	
	if(!p.parse()) {
		std::cerr << "error: can't parse code file.\n";
		code_file.close();
		register_file.close();
		return EXIT_FAILURE;
	}

	std::cout << " -REGISTER FILE = {";
	for(auto r : registers) {
		std::cout << r << " ";
	}
	std::cout << "\b}\n";

	std::cout << " -CODE LISTING = {\n";
	for(auto r : p.code()) {
		std::cout << std::setw(8) << r << "\n";
	}
	std::cout << "  }\n";


	
	e.load(p.code(), registers);
	std::cout << " -LOADED OK.\n";
	e.run(true);
	std::cout << " -HALT.\n";

	std::cout << "\n";
	e.printRegisters();
	std::cout << "\n";
	e.printStats();
	std::cout << "\ndone.\n";


	return 0;
}


void BANNER() {
	std::cout 	<< "### Unlimited Register Machine (URM) ###\n";
	std::cout	<< "© 2022 DeepNet42. All rights reserved.\n";
	std::cout 	<< "(config: REGISTER_FILE_SIZE: " << ISA_REGISTER_FILE_SIZE
				<< ", CODE_LENGTH: " << ISA_CODE_LENGTH << ")\n"
				<< "READY.\n";
}
