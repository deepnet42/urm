/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file common.debug.cpp
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */

# include "common.debug.h"

# include "lexer.h"
# include <iostream>
# include <iomanip>

namespace winkieface {
	std::ostream &operator<<(std::ostream& os, URM_TOKEN_TYPE const& type) {
			std::string tstring;
			switch (type) {
			case URM_TOKEN_TYPE::FUNCTION: tstring = "URM_TOKEN_TYPE::FUNCTION"; break;
			case URM_TOKEN_TYPE::LPAREN: tstring = "URM_TOKEN_TYPE::LPAREN"; break;
			case URM_TOKEN_TYPE::RPAREN: tstring = "URM_TOKEN_TYPE::RPAREN"; break;
			case URM_TOKEN_TYPE::REGISTER: tstring = "URM_TOKEN_TYPE::REGISTER"; break;
			case URM_TOKEN_TYPE::COMMA: tstring = "URM_TOKEN_TYPE::COMMA"; break;
			case URM_TOKEN_TYPE::COLON: tstring = "URM_TOKEN_TYPE::COLON"; break;
			case URM_TOKEN_TYPE::LINE_NUMBER: tstring = "URM_TOKEN_TYPE::LINE_NUMBER"; break;
			case URM_TOKEN_TYPE::COMMENT: tstring = "URM_TOKEN_TYPE::COMMENT"; break;
			case URM_TOKEN_TYPE::NL: tstring = "URM_TOKEN_TYPE::NL"; break;
			case URM_TOKEN_TYPE::WS: tstring = "URM_TOKEN_TYPE::WS"; break;
			case URM_TOKEN_TYPE::UNKNOWN: tstring = "URM_TOKEN_TYPE::UNKNOWN"; break;
			case URM_TOKEN_TYPE::END_TOKEN: tstring = "URM_TOKEN_TYPE:END_TOKEN"; break;
			default: tstring = "???"; break;
			}

			return os << tstring;

		}

		std::ostream &operator<<(std::ostream& os, URMToken const& token) {

				return os 	<< "[" << std::setw(3) << token.index << "," << std::setw(3) << token.row << ":" << std::setw(3) << token.col
							<< "] <" << token.type << ",\"" << (token.value=="\n"? "\\n" : token.value) << "\">";
		}
}
