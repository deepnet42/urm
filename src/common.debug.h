/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file common.debug.h
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */

# ifndef __H__COMMON_DEBUH__H__
# define __H__COMMON_DEBUH__H__


# include "lexer.h"

namespace winkieface {
	std::ostream &operator<<(std::ostream& os, URM_TOKEN_TYPE const& type);
	std::ostream &operator<<(std::ostream& os, URMToken const& token);
}


# endif
