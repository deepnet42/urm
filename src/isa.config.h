/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file isa.config.h
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */

# ifndef __H__ISA__CONFIG__H__
# define __H__ISA__CONFIG__H__


/*
 * Theoretically the URM has an infinite number of registers, but we constrain
 * this. Registers are mapped in a linear fashion. For example,
 * if we specify 10 registers they are index from 0 being the 1st and 9th being the
 * last.
 *
 * All programs on the URM must have a finite but not infinite instruction
 * size. We don't allow an arbitrary program length and constrain it.
 * For example, a code length of 10 means just 10 lines of code is allowed and not more.
 * Increase as needed.
 *
 */

# define ISA_REGISTER_FILE_SIZE 256
# define ISA_CODE_LENGTH 1024


# endif
