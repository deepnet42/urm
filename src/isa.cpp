/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file isa.cpp
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */

# include "isa.h"

namespace winkieface {

	Inst Z(int n) 				{ return Inst{OpCode::Z,n,UNSP,UNSP};	}
	Inst S(int n) 				{ return Inst{OpCode::S,n,UNSP,UNSP};	}
	Inst J(int m, int n, int q)	{ return Inst{OpCode::J,n,m,q};			}
	Inst T(int m, int n)		{ return Inst{OpCode::T,n,m,UNSP};		}




	std::ostream &operator<<(std::ostream& os, OpCode const& opCode) {
		switch(opCode) {
			case OpCode::Z: os << "Z"; break;
			case OpCode::S: os << "S"; break;
			case OpCode::T: os << "T"; break;
			case OpCode::J: os << "J"; break;
			default: os << "?"; break;
		}
		return os;
	}
	std::ostream &operator<<(std::ostream& os, Instruction const& instruction) {
		os << instruction.op << "(";
		switch(instruction.op) {
			case OpCode::S:
			case OpCode::Z:
				os << instruction.n;
				break;
			case OpCode::T:
				os << instruction.m << "," << instruction.n;
				break;
			case OpCode::J:
				os << instruction.m << "," << instruction.n << "," << instruction.q;
				break;
			default:
				os << instruction.m << "," << instruction.n << "," << instruction.q;
				break;
		}
		os << ")";
		return os;
	}

};
