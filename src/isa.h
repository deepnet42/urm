/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file isa.h
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */

# ifndef __H__ISA__H__
# define __H__ISA__H__

/* URM INSTRUCTION SET
 * -------------------
 *
 * ----------------------------------------------------------------------------------------
 * Instruction Name	| Instruction	| Meaning
 * ZERO				| Z(n)			| Set register numbered n to the value of ZERO (0)
 * SUCCESSOR		| S(n)			| Increment register numbered n by ONE (1)
 * JUMP				| J(m,n,q)		| Jump to instruction numbered q if m==n, otherwise
 * 									| proceed to the next instruction
 * TRANSFER			| T(m,n)		| Set the value of register numbered n to the value of
 * 									| the register numbered m.
 * ----------------------------------------------------------------------------------------
 * ----------------------------------------------------------------------------------------
 * Z(n)		| Rn ← 0
 * S(n)		| Rn ← Rn + 1
 * J(m,n,q)	| if (Rm=Rm) ip ← q; otherwise ip ← ip + 1
 * T(m,n)	| Rn ← Rm
 * ----------------------------------------------------------------------------------------
 *
 * */


# include <string>
# include <iostream>

namespace winkieface {

	enum class OpCode : char {
		Z,
		S,
		J,
		T
	};

	struct Instruction {
		OpCode op;
		int n;
		int m;
		int q;
	};

	using Inst = Instruction;

	Inst Z(int n);						// The Zero function
	Inst S(int n);						// The Successor function
	Inst J(int m, int n, int q);		// The Jump function
	Inst T(int m, int n);				// The Transfer function

	const constexpr int UNSP={0};		// padding data used build instructions
										// UNSP for UNSPECIFIED



	std::ostream &operator<<(std::ostream& os, OpCode const& opCode);
	std::ostream &operator<<(std::ostream& os, Instruction const& instruction);
};




# endif
