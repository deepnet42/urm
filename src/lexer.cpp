/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file lexer.cpp
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */
#include "lexer.h"

#include <cctype>

namespace winkieface {

	using TYPE = URM_TOKEN_TYPE;

	lexer::lexer(std::istream* file) :
		file_{file},
		cursor_{0},
		row_{0}, 
		col_{0},
		bcursor_{0}, 
		brow_{0},
		bcol_{0},
		value_{""},
		type_{TYPE::UNKNOWN},
		atend_{false}   {

	}

	std::list<URMToken> lexer::genTokenList(bool ignoreWS) {
		std::list<URMToken> toklist;

		URMToken ptok = {URM_TOKEN_TYPE::UNKNOWN, "",0 ,0 ,0};
		for(URMToken tok = nextToken(); tok.type != URM_TOKEN_TYPE::END_TOKEN; tok=nextToken()) {
			
			if(ignoreWS and tok.type == URM_TOKEN_TYPE::WS)
				continue;
			
			if(ptok.type == URM_TOKEN_TYPE::REGISTER and tok.type == URM_TOKEN_TYPE::COLON) {
				//the lexer still sends LINE_NUMBER token to the parser
				toklist.back().type = URM_TOKEN_TYPE::LINE_NUMBER;
			} else {
				toklist.push_back(tok);
			}
			
			ptok = tok;
			
		}


		return toklist;
	}


	void lexer::lex() {

		bcursor_ = cursor_;
		brow_ = row_;
		bcol_ = col_;

		char ch = std::toupper(getch());

		switch(ch) {
		case M_Z:		set(TYPE::FUNCTION, ch); break;
		case M_S:		set(TYPE::FUNCTION, ch); break;
		case M_J:		set(TYPE::FUNCTION, ch); break;
		case M_T:		set(TYPE::FUNCTION, ch); break;
		case S_LPAREN:	set(TYPE::LPAREN, ch); break;
		case S_RPAREN:	set(TYPE::RPAREN, ch); break;
		case S_COMMA:	set(TYPE::COMMA, ch); break;
		case S_COLON:	set(TYPE::COLON, ch); break;

		case NL:		row_++; col_=0; set(TYPE::WS, ch); break;
		
		case SPACE:			
		case CR:		
		case HTAB:		set(TYPE::WS, ch); break;

		case '0': 
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':		lexInteger(ch); break;

		case ';':		lexComment(ch); break;
		case EOF:		set(TYPE::END_TOKEN, ch); break;
		default:		set(TYPE::UNKNOWN, ch); break;


		}
	}

	void lexer::set(URM_TOKEN_TYPE type, std::string value) {
		type_ = type;
		value_ = value;
	}

	void lexer::set(URM_TOKEN_TYPE type, char ch) {
		type_ = type;
		value_ = ch;
	}


	void lexer::lexInteger(char ch) {

		std::string integerstr;
		integerstr.push_back(ch);


		while(!atend() and std::isdigit(peekch())) {
			integerstr.push_back(getch());
		}

		set(TYPE::REGISTER, integerstr);
	}

	void lexer::lexComment(char ch) {
		std::string comment;
		comment.push_back(ch);

		while(!atend() and peekch()!=NL) {
			comment.push_back(getch());
		}

		set(TYPE::COMMENT, comment);
	}

	char lexer::getch() {

		char ch  = file_->get();

		if (ch == EOF) {
			atend_=true;
		}

		cursor_++;
		col_++;
		return ch;
	}


	char lexer::peekch() {
		char ch = file_->peek();
		return ch;
	}

	bool lexer::atend() {
		return atend_;
	}

	URMToken lexer::nextToken() {
		lex();
		return URMToken{type_,value_,bcursor_,brow_,bcol_};
	}


}