/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file lexer.driver.cpp
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */

# include "lexer.h"
# include "common.debug.h"

# include <iostream>
# include <fstream>

int main(int argc, char* argv[]) {

	if(argc < 2) {
		std::cout << argv[0] << " source" << std::endl;
		return EXIT_FAILURE;
	}


	std::ifstream file(argv[1]);
	if(file.is_open()) {
		winkieface::lexer lex(&file);
		winkieface::URMToken tok;

		std::list<winkieface::URMToken> tokens = lex.genTokenList();
		for(winkieface::URMToken& t : tokens) {
			std::cout << t << std::endl;
		}

		file.close();
	}

	return EXIT_SUCCESS;
}
