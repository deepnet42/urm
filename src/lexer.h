/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file lexer.h
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */

#ifndef __H__LEXER__H__
#define __H__LEXER__H__



# include <istream>
# include <list>

namespace winkieface {

	enum class URM_TOKEN_TYPE {
		FUNCTION,				/* Z,S,J,T */
		LPAREN,
		RPAREN,
		REGISTER,				/* natural number */
		NL,
		WS,
		UNKNOWN,
		COMMA,
		LINE_NUMBER,
		COLON,
		COMMENT,
		END_TOKEN
	};


	const char M_Z{'Z'};
	const char M_S{'S'};
	const char M_J{'J'};
	const char M_T{'T'};

	const char S_LPAREN	{'('};
	const char S_RPAREN	{')'};
	const char S_COMMA	{','};
	const char S_COLON	{':'};
	const char S_SEMICOLON {';'};

	const char SPACE	{' '};
	const char NL		{'\n'};
	const char CR		{'\r'};
	const char HTAB		{'\t'};

	struct URMToken {
		URM_TOKEN_TYPE type;	/* symbolic type/category */
		std::string value;		/* actual value */

		int index,row,col;		/* used for error reporting by the lexer and the parser */
	};




	class lexer {
	public:


		lexer(std::istream* file);
		std::list<URMToken> genTokenList(bool ignoreWS=false);



	private:


		void lex();

		char getch();
		char peekch();
		bool atend();

		void set(URM_TOKEN_TYPE type, std::string value);
		void set(URM_TOKEN_TYPE type, char value);

		void lexInteger(char ch);
		void lexComment(char ch);

		URMToken nextToken();
	private:


		std::istream* file_;

		int cursor_;
		int row_;
		int col_;

		int bcursor_;
		int brow_;
		int bcol_;

		std::string value_;
		URM_TOKEN_TYPE type_;

		bool atend_ = false;


	};







};
#endif /* LEXER_H_ */
