/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file parser.cpp
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */
# include "parser.h"
# include "isa.h"

# include <string>
# include <iostream>
# include <vector>
# include <list>

namespace winkieface {




		parser::parser(lexer* lex) : lex_(lex) {

		}



		bool parser::parse() {
			constexpr bool ignorews = true;
			tokens = lex_->genTokenList(ignorews);

			while(!tokens.empty()) {
				if(!parseLine()) {
					return false;
				}
			}


			return true;
		}

		std::vector<Inst> parser::code() {
			return code_;
		}


		bool parser::parseLine() {
			//std::cout << __FUNCTION__ << "\n";
			URMToken t = tokens.front(); tokens.pop_front();

			if(t.type == URM_TOKEN_TYPE::LINE_NUMBER) {
				// ignore line numbers as they are strictly cosmetic
				// std::cout << "[debug] [line number] [ignore] " << t.value << "\n";
				return ignoreTokenSuccess();
			} else if (t.type == URM_TOKEN_TYPE::FUNCTION) {
				//std::cout<<"FUNCTION(Z,S,T,J)\n";
				return parseFunction(t);
			} else if (t.type == URM_TOKEN_TYPE::COMMENT) {
				// ignore comments as well
				// std::cout << "[debug] [comment] [ignore] " << t.value << "\n";
				return ignoreTokenSuccess();
			}

			reportError(t.index, t.row, t.row, __LINE__ , __FUNCTION__,
						"expected start of mnemonic name.",
						t.value);

			return false;
		}

		bool parser::parseFunction(URMToken& t) {
			//std::cout << __FUNCTION__ << "\n";
			if		(t.value == "Z") { return parseFuncZero();}
			else if	(t.value == "S") { return parseFuncSuccessor();}
			else if (t.value == "J") { return parseFuncJump();}
			else if (t.value == "T") { return parseFuncTransfer();}

			//std::cout << __FUNCTION__ << " FAIL \n";
			return false;
		}

		bool parser::parseFuncZero() {
			//std::cout << __FUNCTION__ << "\n";
			if(tokens.size()>=3) {
				URMToken toks[3];
				for(int i=0;i<3;i++) { toks[i] = tokens.front(); tokens.pop_front();}

				if(	toks[0].type == URM_TOKEN_TYPE::LPAREN and
					toks[1].type == URM_TOKEN_TYPE::REGISTER and
					toks[2].type == URM_TOKEN_TYPE::RPAREN) {

					code_.push_back(Z(std::stoi(toks[1].value)));
					return true;
				}

				//std::cout << __FUNCTION__ << "FAIL \n";
				return false;

			}

			//std::cout << __FUNCTION__ << " FAIL \n";
			return false;
		}

		bool parser::parseFuncSuccessor() {
			//std::cout << __FUNCTION__ << "\n";
			if(tokens.size()>=3) {
				URMToken toks[3];
				for(int i=0;i<3;i++) { toks[i] = tokens.front(); tokens.pop_front();}

				if(	toks[0].type == URM_TOKEN_TYPE::LPAREN and
					toks[1].type == URM_TOKEN_TYPE::REGISTER and
					toks[2].type == URM_TOKEN_TYPE::RPAREN) {
					code_.push_back(S(std::stoi(toks[1].value)));
					return true;
				}

				//std::cout << __FUNCTION__ << "FAIL \n";
				return false;

			}

			//std::cout << __FUNCTION__ << "FAIL \n";
			return false;

		}
		bool parser::parseFuncJump() {
			//std::cout << __FUNCTION__ << "\n";
			if(tokens.size()>=7) {
				URMToken toks[7];
				for(int i=0;i<7;i++) { toks[i] = tokens.front(); tokens.pop_front();}

				if(	toks[0].type == URM_TOKEN_TYPE::LPAREN and
					toks[1].type == URM_TOKEN_TYPE::REGISTER and
					toks[2].type == URM_TOKEN_TYPE::COMMA and
					toks[3].type == URM_TOKEN_TYPE::REGISTER and
					toks[4].type == URM_TOKEN_TYPE::COMMA and
					toks[5].type == URM_TOKEN_TYPE::REGISTER and //NUMBER?
					toks[6].type == URM_TOKEN_TYPE::RPAREN) {

					code_.push_back(J(std::stoi(toks[1].value),std::stoi(toks[3].value),std::stoi(toks[5].value)));

					return true;
				}

				//std::cout << __FUNCTION__ << "FAIL \n";
				return false;

			}

			//std::cout << __FUNCTION__ << "FAIL \n";
			return false;


		}
		bool parser::parseFuncTransfer() {
			//std::cout << __FUNCTION__ << "\n";
			if(tokens.size()>=5) {
				URMToken toks[5];
				for(int i=0;i<5;i++) { toks[i] = tokens.front(); tokens.pop_front();}

				if(	toks[0].type == URM_TOKEN_TYPE::LPAREN and
					toks[1].type == URM_TOKEN_TYPE::REGISTER and
					toks[2].type == URM_TOKEN_TYPE::COMMA and
					toks[3].type == URM_TOKEN_TYPE::REGISTER and
					toks[4].type == URM_TOKEN_TYPE::RPAREN) {

					code_.push_back(T(std::stoi(toks[1].value),std::stoi(toks[3].value)));

					return true;
				}

				//std::cout << __FUNCTION__ << "FAIL \n";
				return false;

			}

			//std::cout << __FUNCTION__ << "FAIL \n";
			return false;

		}

		bool parser::ignoreTokenSuccess() {
			return true;
		}

		void parser::reportError(	int tcur, int trow, int tcol,
									int callerline, const char* functionname,
									std::string errormessage,
									std::string suggestion) {


			std::cerr 	<< "[error ][parser][" << tcur << "." << trow << ":" << tcol << "] "
						<< errormessage << "\n"
						<< "             GOT: " << suggestion << "\n"
						<< "[^debug]         " << functionname << ":" << callerline << "\n";
		}


		void parser::reportTrace(	int tcur, int trow, int tcol,
									int callerline, const char* functionname) {

			std::cerr 	<< "[trace ][parser][" << tcur << "." << trow << ":" << tcol << "]\n"
						<< "[^debug]         " << functionname << ":" << callerline << "\n";

		}


};
