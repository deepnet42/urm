/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file parser.driver.h
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */

# include "parser.h"

# include <iostream>
# include <fstream>

int main(int argc, char* argv[]) {


	if(argc < 2) {
		std::cout << argv[0] << " source" << std::endl;
		return EXIT_FAILURE;
	}


	int pass = false;
	std::ifstream file(argv[1]);
	if(file.is_open()) {
		winkieface::lexer lex(&file);
		winkieface::URMToken tok;
		winkieface::parser par(&lex);
		pass = par.parse();
		std::cout << (pass?"SUCCESS\n":"FAIL\n");
		file.close();
	}

	return (pass?EXIT_SUCCESS:EXIT_FAILURE);


}
