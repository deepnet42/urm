/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file parser.h
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */

# ifndef __H__PARSER__H__
# define __H__PARSER__H__

# include "lexer.h"
# include "isa.h"

# include <list>
# include <vector>

namespace winkieface {


	/*
	 * PROGRAM		-> LINE*
	 * LINE			-> Z | S | J | T
	 * Z			-> "Z(" R ")"
	 * S			-> "S(" R ")"
	 * J			-> "J(" R "," R "," NUMBER ")"
	 * T			-> "T(" R "," R ", ")"
	 * R			-> NUMBER
	 * NUMBER		-> [0-9]+
	 */


	class parser {
		public:

		parser(lexer* lex);

		bool parse();

		std::vector<Inst> code();



		private:

		void reportError(	int tcur, int trow, int tcol,
							int callerline, const char* functionname,
							std::string errormessage,
							std::string suggestion);

		void reportTrace(	int tcur, int trow, int tcol,
							int callerline, const char* functionname);
		bool parseLine();
		bool parseFunction(URMToken& t);

		bool parseFuncZero();
		bool parseFuncSuccessor();
		bool parseFuncJump();
		bool parseFuncTransfer();

		bool ignoreTokenSuccess();

		lexer* lex_;
		std::list<winkieface::URMToken> tokens;
		std::vector<Inst> code_;


	};
};



# endif
