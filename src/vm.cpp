/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file vm.cpp
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */

# include "vm.h"

# include <iostream>
# include <sstream>
# include <exception>
# include <iomanip>
# include <cmath>
# include <algorithm>

namespace winkieface {

	VMEngine::VMEngine(	int maxRegisters, int maxCodeLength) :
		code_{},
		ip_{CODE_START}, 
		ir_{},
		maxRegisters_{maxRegisters+CODE_START},
		maxCodeLength_{maxCodeLength+CODE_START},
		halted_{false},
		stats{},
		realtimetrace_{false},
		accessedRegisters{}
		{

			registers_.reserve(maxRegisters_);
			registers_.assign(maxRegisters_,0);

		}

	bool VMEngine::load(const std::vector<Inst> code, const std::vector<int> registers) {
		if (	code.size() > maxCodeLength_ and
				registers.size() > maxRegisters_) {
			return false;
		}


		code_.clear();
		code_.push_back(Z(0)); // we start instructions at 1, not 0; just dummy data
		for(auto line : code) {
			code_.push_back(line);
		}

		
		registers_.clear();
		registers_.push_back(0); // unused
		for(auto r : registers) {
			registers_.push_back(r);
		}	
		
		

		std::fill_n(std::back_inserter(registers_), maxRegisters_ - registers_.size(), 0);
		accessedRegisters.clear();

		// add checks later
		return true;
	}


	void VMEngine::run(bool realtimetrace) {
		realtimetrace_ = realtimetrace;
		while(step()) {
			
		}
	}

	bool VMEngine::step() {
		//std::cout << "ip:" << ip_ << " ";

		
		loadI();
		if(halted_) {
			return false;
		}

		if(realtimetrace_)
			trace();

		decodeI();
		execI();

		return true;
	}


	void VMEngine::loadI() {
		if (ip_ < code_.size() and ip_ >= CODE_START) {
			ir_ = code_[ip_++];
		} else {
			if(ip_ == HALT_MAGIC) {
				if(realtimetrace_) std::cout << "!!!Explicit HALT !!!\n";
			} else {
				if(realtimetrace_) std::cout << "!!!END OF PROGRAM!!!\n";
			}
			halted_=true;
		}
	}
	void VMEngine::decodeI() {
		// no special decode operations needed
	}
	void VMEngine::execI() {
		switch(ir_.op) {
		case OpCode::Z: execZero(); stats.eZ++; break;
		case OpCode::S: execSuccessor(); stats.eS++; break;
		case OpCode::T: execTranfer(); stats.eT++; break;
		case OpCode::J: execJump(); stats.eJ++; break;
		default: break;
		};
		stats.ticks++;
	}


	void VMEngine::execZero() {
		if(ir_.n > 0 and ir_.n < maxRegisters_) {
			registers_.at(ir_.n) = 0;
			accessedRegisters.insert(ir_.n);
			if(realtimetrace_) std::cout << std::setw(16) << " " << "R" << ir_.n << " ← " << 0 << " (R" << ir_.n << "==" << registers_.at(ir_.n) << ")\n";
		} else {
			{
				std::stringstream ss;
				ss	<< "[exception] [ " << __FILE__ << ":" << __LINE__ << " in "<< __func__ << "]\n";
				ss	<< getDebugInfo();
				ss	<< "[  details] [ " << "bad register reference.\n";
				ss	<< "[   reason] [ " << "ir_.n > 0 and ir_.n < maxRegisters_\n";
				throw std::runtime_error(ss.str());
			}
		}
		//std::cout << "Z\n";
	}

	void VMEngine::execSuccessor() {
		if(ir_.n > 0 and ir_.n < maxRegisters_) {
			registers_.at(ir_.n) = registers_.at(ir_.n) + 1;
			accessedRegisters.insert(ir_.n);
			if(realtimetrace_) std::cout << std::setw(16) << " " << "R" << ir_.n << " ← " << "R" << ir_.n << " + 1 " << "(R" << ir_.n << "==" << registers_.at(ir_.n) << ")\n";
		} else {
			{
				std::stringstream ss;
				ss	<< "[exception] [ " << __FILE__ << ":" << __LINE__ << " in "<< __func__ << "]\n";
				ss	<< getDebugInfo();
				ss	<< "[  details] [ " << "bad register reference.\n";
				ss	<< "[   reason] [ " << "ir_.n > 0 and ir_.n < maxRegisters_\n";
				throw std::runtime_error(ss.str());
			}
		}
		//std::cout << "S\n";
	}
	void VMEngine::execTranfer() {
		if(	ir_.n > 0 and ir_.n < maxRegisters_ and
			ir_.m > 0 and ir_.m < maxRegisters_) {
				registers_.at(ir_.n) = registers_.at(ir_.m);
				accessedRegisters.insert(ir_.n);
				accessedRegisters.insert(ir_.m);
				if(realtimetrace_) std::cout << std::setw(16) << " " << "R" << ir_.m << " ← " << "R" << ir_.n << " (R" << ir_.n << "==" << registers_.at(ir_.n) << ")\n";
		} else {
			{
				std::stringstream ss;
				ss	<< "[exception] [ " << __FILE__ << ":" << __LINE__ << " in "<< __func__ << "]\n";
				ss	<< getDebugInfo();
				ss	<< "[  details] [ " << "bad register reference.\n";
				ss	<< "[   reason] [ " << "ir_.n > 0 and ir_.n < maxRegisters_ and ir_.m > 0 and ir_.m < maxRegisters_\n";
				throw std::runtime_error(ss.str());
			}
		}
		//std::cout << "T\n";
	}
	void VMEngine::execJump() {
		if(	ir_.n > 0 and ir_.n < maxRegisters_ and
			ir_.m > 0 and ir_.m < maxRegisters_ and 
			(ir_.q >= CODE_START and ir_.q <= maxCodeLength_) or ir_.q == HALT_MAGIC) {
				accessedRegisters.insert(ir_.n);
				accessedRegisters.insert(ir_.m);
				if(registers_.at(ir_.m) == registers_.at(ir_.n)) {
					ip_ = ir_.q;
					if(realtimetrace_) std::cout << std::setw(16) << " " << "ip " << " ← " << ir_.q << " (Jump to line # " << ir_.q << ")\n";
				} else {
					if(realtimetrace_) std::cout << std::setw(16) << " " << "ip " << " ← " << ip_ << " (Execute next instruction)\n";
					//nothing
				}
		} else {
			{
				std::stringstream ss;
				ss	<< "[exception] [ " << __FILE__ << ":" << __LINE__ << " in "<< __func__ << "]\n";
				ss	<< getDebugInfo();
				ss	<< "[  details] [ " << "bad register reference or jump line\n";
				ss	<< "[   reason] [ " << "ir_.n > 0 and ir_.n < maxRegisters_ and ir_.m > 0 and ir_.m < maxRegisters_ and ir_.q >= CODE_START and ir_.q <= maxCodeLength_\n";
				throw std::runtime_error(ss.str());
			}
		}
		//std::cout << "J\n";
	}

	void VMEngine::printRegisters() {
		for(auto i : accessedRegisters) {
			std::cout << "R[" << i << "] = " << registers_.at(i) << "\n";
		}
		std::cout<< "\n";
	}

	void VMEngine::printStats() {
		std::cout 	<< "stats { ticks:"<< stats.ticks
					<< " e { Z:" << stats.eZ
					<< " S:" << stats.eS
					<< " T:" << stats.eT
					<< " J:" << stats.eJ
					<< "} }\n";
	}

	std::string VMEngine::getDebugInfo() {
		std::stringstream ss;
		ss	<< "[    debug] [ ip = " << ip_-1 << "\n";
		ss	<< "[    debug] [ ir = " << ir_ << "\n";
		return ss.str();
	}

	void VMEngine::trace() {
		std::cout << "[" << std::setw(std::log2(code_.size())) << ip_-1 << "] " << ir_ << " "; 
	}

};
