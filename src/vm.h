/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/**
 * \author DeepNet42
 * \file vm.h
 * \version 0
 * \date 2022
 * \copyright (c) 2022 DeepNet42. All rights reserved.
 * This project is released under the Mozilla Public License v. 2.0.	 
 */

# ifndef __H__VM__H__
# define __H__VM__H__

# include <vector>
# include <set>

# include "isa.h"
# include "isa.config.h"

namespace winkieface {

	/*
	 * \class VMEngine
	 *
	 * \note
	 * This project implements an unlimited register machine (URM) as specified by N.J. Cutland in a 
	 * book called Computability: An introduction to Recursive Function Theory (p9-16). 
	 * Furthermore, this model is based on Shepherdson and Sturgis. 
	 * (see wiki: https://en.wikipedia.org/wiki/Counter-machine_model#1963:_Shepherdson_and_Sturgis's_model)}
	 * 
	 * 
	 * A virtual machine engine for a unlimited register machine. See isa.h for more information on
	 * how URM works.
	 *
	 * URM machines have an unlimited register space which can store numbers of arbitrary size, but a real
	 * machine would have to have reasonable limits due to physical limitations. However, it doesn't make it
	 * less powerful and the user can expand for more register space and the code length as desired.
	 * Limits are specified where in as a security measure.
	 * 
	 * VMEngine requires the user to provide it with code instructions and initial input. The user may
	 * choose to embed input as part of the instructions if that suits the task better.
	 *
	 * VMEengine allows the user to run a URM program until it terminates. It is impossible to determine
	 * if a program will ever terminate or not since URMs computationally universal and are equivalent to
	 * a Turing Machine.
	 *
	 * Another way to run a program is to step through it step by step which would allow the user to
	 * inspect changes to internal registers of the machine and take note of execution trace for debug
	 * purposes.
	 */

	class VMEngine {
	public:

		static constexpr int MAX_REGISTERS_DEFAULT		{ISA_REGISTER_FILE_SIZE};		// in number of register units
		static constexpr int MAX_CODE_LENGTH_DEFAULT	{ISA_CODE_LENGTH};				// in lines of code (instructions)
		static constexpr int CODE_START					{1};
		static constexpr int OFFSET						{1};
		static constexpr int HALT_MAGIC					{0};

	public:
		VMEngine(	int maxRegisters	= MAX_REGISTERS_DEFAULT,
					int maxCodeLength	= MAX_CODE_LENGTH_DEFAULT
				);

		bool load(const std::vector<Inst> code, const std::vector<int> registers);
		bool step();
		void run(bool realtimetrace=false);

		void printRegisters();
		void printStats();

	protected:

		void loadI();
		void decodeI();
		void execI();

		void halt();


		void execZero();				/// Maps to Z(n) 	| Rn ← 0
		void execSuccessor();			/// Maps to S(n) 	| Rn ← Rn + 1
		void execTranfer();				/// Maps to T(m,n) 	| Rn ← Rm
		void execJump();				/// Maps to J(m,n,q)| ip ← q if Rm=Rn else ip ← ip + 1

		std::string getDebugInfo();
		
		void trace();

	private:


		std::vector<Inst> code_;

		int ip_;
		Inst ir_;
		std::vector<int> registers_;



		int maxRegisters_;
		int maxCodeLength_;


		bool halted_;


		struct {
			int ticks;
			int eZ;
			int eS;
			int eT;
			int eJ;
		} stats;

		bool realtimetrace_;
		std::set<int> accessedRegisters;

	};
};

# endif
